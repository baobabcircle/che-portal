/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme")
module.exports = {
	content: ["./src/**/*.{js,ts,jsx,tsx,mdx}"],
	theme: {
		extend: {
			screens: {
				xxs: "375px",
				xs: "475px",
				...defaultTheme.screens,
				md: { min: "768px" },
				lg: { min: "1200px" },
				sm: { max: "767px" },
				xl: "1299px",
				xxl: "1450px",
			},
			backgroundImage: {
				"gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
				"gradient-conic":
					"conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
				"custom-gradient":
					"linear-gradient(90deg, rgba(241, 155, 62, 0.1), rgba(241, 155, 62, 0))",
				"custom-gradient-sm":
					"linear-gradient(0deg, rgba(241, 155, 62, 0.1), rgba(241, 155, 62, 0))",
			},

			boxShadow: {
				"inner-custom": "inset 0px 0px 0px 1px rgba(217, 217, 217, 1)",
				"inner-custom-border":
					"inset 0px 0px 0px 1px rgba(241, 155, 62, 1)",
			},
		},
	},
	plugins: [],
}
