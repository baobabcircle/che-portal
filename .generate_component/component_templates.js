// component.jsx
exports.component = (name) => `import React from 'react';
import styles from './${name}.module.scss';
const ${name} = () => {
  return <div>Hello 👋, I am a ${name} component.</div>;
};
export default ${name};
`

// component.stories.jsx
exports.story = (name) => `import React from 'react';
import ${name} from './${name}.jsx';
export default {
  title: '${name}',
  component: ${name},
};
export const Default = () => <${name} />;
`

// component.test.jsx
exports.test = (name) => `import React from 'react';
import { render } from '@testing-library/react';
import ${name} from './${name}';
describe('${name} Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<${name} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
`

// index.ts
exports.barrel = (name) => `import ${name} from './${name}';
export default ${name};
`
