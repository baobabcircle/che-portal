import React from 'react';
import TileButton from './TileButton.jsx';
export default {
  title: 'TileButton',
  component: TileButton,
};
export const Default = () => <TileButton />;
