import React from 'react';
import { render } from '@testing-library/react';
import TileButton from './TileButton';
describe('TileButton Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<TileButton />);
    expect(asFragment()).toMatchSnapshot();
  });
});
