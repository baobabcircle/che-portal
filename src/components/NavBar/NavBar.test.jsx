import React from 'react';
import { render } from '@testing-library/react';
import NavBar from './NavBar';
describe('NavBar Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<NavBar />);
    expect(asFragment()).toMatchSnapshot();
  });
});
