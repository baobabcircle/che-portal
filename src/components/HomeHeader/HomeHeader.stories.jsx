import React from 'react';
import HomeHeader from './HomeHeader.jsx';
export default {
  title: 'HomeHeader',
  component: HomeHeader,
};
export const Default = () => <HomeHeader />;
