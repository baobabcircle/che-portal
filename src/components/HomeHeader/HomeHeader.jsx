import React, { useEffect, useState } from "react"
import AlertButton from "../AlertButton/AlertButton"
import styles from "./HomeHeader.module.scss"
const HomeHeader = () => {
	const [time, setTime] = useState("")
	useEffect(() => {
		const hours = new Date().getHours().toLocaleString()
		if (hours < 12) {
			setTime("Good Morning")
		} else if (hours < 18) {
			setTime("Good Afternoon")
		} else if (hours > 18) {
			setTime("Good Evening")
		} else {
			setTime("Hello")
		}
	}, [])
	return (
		<div className={styles.header}>
			<div className={styles.left}>
				<h4>{time}</h4>
				<h1>CHE</h1>
				<p>Welcome to CHE portal</p>
			</div>
			<div className={styles.right}>
				<AlertButton />
			</div>
		</div>
	)
}
export default HomeHeader
