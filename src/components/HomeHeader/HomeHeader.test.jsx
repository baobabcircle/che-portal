import React from 'react';
import { render } from '@testing-library/react';
import HomeHeader from './HomeHeader';
describe('HomeHeader Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<HomeHeader />);
    expect(asFragment()).toMatchSnapshot();
  });
});
