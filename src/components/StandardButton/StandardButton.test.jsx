import React from 'react';
import { render } from '@testing-library/react';
import StandardButton from './StandardButton';
describe('StandardButton Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<StandardButton />);
    expect(asFragment()).toMatchSnapshot();
  });
});
