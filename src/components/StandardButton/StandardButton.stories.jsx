import React from 'react';
import StandardButton from './StandardButton.jsx';
export default {
  title: 'StandardButton',
  component: StandardButton,
};
export const Default = () => <StandardButton />;
