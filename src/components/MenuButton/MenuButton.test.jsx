import React from 'react';
import { render } from '@testing-library/react';
import MenuButton from './MenuButton';
describe('MenuButton Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<MenuButton />);
    expect(asFragment()).toMatchSnapshot();
  });
});
