import React from 'react';
import MenuButton from './MenuButton.jsx';
export default {
  title: 'MenuButton',
  component: MenuButton,
};
export const Default = () => <MenuButton />;
