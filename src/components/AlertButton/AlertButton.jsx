import React from "react"
import styles from "./AlertButton.module.scss"
const AlertButton = () => {
	return (
		<button className={styles.alert_button}>
			<div className={styles.icon}>
				<svg
					width="50"
					height="50"
					viewBox="0 0 50 50"
					fill="none"
					xmlns="http://www.w3.org/2000/svg"
				>
					<circle cx="25" cy="25" r="25" fill="#E9E9E9" />
					<path
						d="M24.8462 37C26.2 37 27.3077 35.8923 27.3077 34.5385H22.3846C22.3846 35.8923 23.4923 37 24.8462 37ZM32.2308 29.6154V23.4615C32.2308 19.6831 30.2246 16.52 26.6923 15.6831V14.8462C26.6923 13.8246 25.8677 13 24.8462 13C23.8246 13 23 13.8246 23 14.8462V15.6831C19.48 16.52 17.4615 19.6708 17.4615 23.4615V29.6154L15 32.0769V33.3077H34.6923V32.0769L32.2308 29.6154ZM29.7692 30.8462H19.9231V23.4615C19.9231 20.4092 21.7815 17.9231 24.8462 17.9231C27.9108 17.9231 29.7692 20.4092 29.7692 23.4615V30.8462Z"
						fill="#444445"
					/>
				</svg>
			</div>
		</button>
	)
}
export default AlertButton
