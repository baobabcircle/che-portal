import React from 'react';
import { render } from '@testing-library/react';
import ContactItem from './ContactItem';
describe('ContactItem Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<ContactItem />);
    expect(asFragment()).toMatchSnapshot();
  });
});
