import React from 'react';
import BackButton from './BackButton.jsx';
export default {
  title: 'BackButton',
  component: BackButton,
};
export const Default = () => <BackButton />;
