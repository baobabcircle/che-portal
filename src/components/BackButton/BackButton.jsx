import React from "react"
import styles from "./BackButton.module.scss"
const BackButton = ({ onClick }) => {
	return (
		<button className={styles.back_button} onClick={onClick}>
			<div className={styles.icon}>
				<svg
					viewBox="0 0 24 24"
					fill="none"
					xmlns="http://www.w3.org/2000/svg"
				>
					<g clipPath="url(#clip0_1_7810)">
						<path
							d="M20 10H7.8L13.4 4.4L12 3L4 11L12 19L13.4 17.6L7.8 12H20V10Z"
							fill="#141720"
							stroke="#141720"
							strokeWidth="0.5"
						/>
					</g>
				</svg>
			</div>
		</button>
	)
}
export default BackButton
