import React from 'react';
import { render } from '@testing-library/react';
import BackButton from './BackButton';
describe('BackButton Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<BackButton />);
    expect(asFragment()).toMatchSnapshot();
  });
});
