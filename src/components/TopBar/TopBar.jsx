import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router"
import { useLocation } from "react-router-dom"
import BackButton from "../../components/BackButton/BackButton"
import styles from "./TopBar.module.scss"
const TopBar = () => {
	const navigate = useNavigate()
	const location = useLocation()

	const [hidden, setHidden] = useState(true)
	const [title, setTitle] = useState("Title")
	useEffect(() => {
		if (location.pathname === "/") {
			setHidden(true)
		} else {
			setTitle(
				location.pathname.split("/").slice(-1)[0].replace(/-|_/, " ")
			)
			setHidden(false)
		}
	}, [location])
	return (
		<div
			className={styles.top_bar}
			style={{ display: hidden ? "none" : "grid" }}
		>
			<div className={styles.control}>
				<BackButton onClick={() => navigate(-1)} />
			</div>
			<div className={styles.title}>
				<p>{title}</p>
			</div>
		</div>
	)
}
export default TopBar
