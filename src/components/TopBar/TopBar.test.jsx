import React from 'react';
import { render } from '@testing-library/react';
import TopBar from './TopBar';
describe('TopBar Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<TopBar />);
    expect(asFragment()).toMatchSnapshot();
  });
});
