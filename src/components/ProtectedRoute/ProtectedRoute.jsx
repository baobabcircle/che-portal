import React from "react"
import { Navigate } from "react-router-dom"
import styles from "./ProtectedRoute.module.scss"
const ProtectedRoute = ({ user, children }) => {
	if (!user) {
		return <Navigate to="/login" replace />
	}

	return children
}
export default ProtectedRoute
