import React from 'react';
import { render } from '@testing-library/react';
import ProtectedRoute from './ProtectedRoute';
describe('ProtectedRoute Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<ProtectedRoute />);
    expect(asFragment()).toMatchSnapshot();
  });
});
