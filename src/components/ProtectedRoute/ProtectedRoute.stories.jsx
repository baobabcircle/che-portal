import React from 'react';
import ProtectedRoute from './ProtectedRoute.jsx';
export default {
  title: 'ProtectedRoute',
  component: ProtectedRoute,
};
export const Default = () => <ProtectedRoute />;
