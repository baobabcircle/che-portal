import React from 'react';
import { render } from '@testing-library/react';
import CallScreen from './CallScreen';
describe('CallScreen Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<CallScreen />);
    expect(asFragment()).toMatchSnapshot();
  });
});
