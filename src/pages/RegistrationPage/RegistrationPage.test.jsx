import React from 'react';
import { render } from '@testing-library/react';
import RegistrationPage from './RegistrationPage';
describe('RegistrationPage Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<RegistrationPage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
