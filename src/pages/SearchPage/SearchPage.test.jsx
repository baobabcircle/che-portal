import React from "react"
import { render } from "@testing-library/react"
import SearchPage from "./SearchPage"
describe("SearchPage Page", () => {
	test("it should match the snapshot", () => {
		const { asFragment } = render(<SearchPage />)
		expect(asFragment()).toMatchSnapshot()
	})
})
