import React, { useEffect, useState, useRef } from "react"
import { useNavigate } from "react-router"

const Login = () => {
	const [referral, setReferral] = useState("")
	const [country, setCountry] = useState("254")
	const [phone, setPhone] = useState("")
	const [error, setError] = useState("")
	const [loading, setLoading] = useState(false)
	const phoneRef = useRef(null)

	const navigate = useNavigate()

	const handleCountry = (event) => {
		setCountry(event.target.value)
	}
	const handlePhone = (event) => {
		setPhone(event.target.value.replace(/^0/, ""))
	}

	const handleSubmit = async (e) => {
		e.preventDefault()
		setLoading(true)
		await fetch(
			`${process.env.REACT_APP_BASE_URL}/api/calendar/login/code`,
			{
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					code: referral,
					phone: "+" + country + phone,
				}),
			}
		)
			.then((response) => response.json())
			.then((result) => {
				if (result.success) {
					setLoading(false)
					navigate(`/confirmation/${country + phone}`)
				} else {
					setLoading(false)
					throw new Error(result.message)
				}
			})
			.catch((error) => {
				setLoading(false)
				setError(error.message)
			})
	}
	useEffect(() => {
		return function () {
			window.scrollTo(0, 0)
		}
	}, [])

	return (
		<div className="h-full w-full flex flex-col">
			<div className="mx-auto py-[25px]">
				<svg
					className="h-[25px]"
					viewBox="0 0 215 25"
					fill="none"
					xmlns="http://www.w3.org/2000/svg"
				>
					<g clipPath="url(#clip0_29_7485)">
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M132.771 12.5331C132.771 7.97908 136.173 4.86859 140.55 4.86859C143.55 4.86859 145.396 6.42424 146.507 8.20164L144.285 9.3347C143.573 8.11293 142.15 7.17988 140.55 7.17988C137.616 7.17988 135.438 9.42341 135.438 12.5331C135.438 15.6428 137.615 17.8879 140.55 17.8879C141.306 17.8837 142.048 17.6826 142.702 17.3045C143.357 16.9263 143.902 16.3842 144.285 15.7315L146.507 16.8645C145.374 18.642 143.551 20.1968 140.55 20.1968C136.173 20.1968 132.771 17.0863 132.771 12.5331ZM150.374 19.9299H152.976V5.11295H150.374V19.9299ZM166.6 9.75567C166.6 8.3339 165.533 7.40085 164.065 7.40085H160.509V12.1105H164.065C165.533 12.1105 166.6 11.1783 166.6 9.75567ZM166.377 19.9299L163.087 14.3992H160.509V19.9299H157.909V5.11295H164.422C167.356 5.11295 169.267 7.02424 169.267 9.75567C169.267 12.3992 167.533 13.821 165.755 14.1105L169.377 19.9299H166.377ZM172.978 12.5331C172.978 7.97908 176.38 4.86859 180.757 4.86859C183.758 4.86859 185.604 6.42424 186.715 8.20164L184.493 9.3347C183.781 8.11293 182.358 7.17988 180.757 7.17988C177.823 7.17988 175.646 9.42341 175.646 12.5331C175.646 15.6428 177.823 17.8879 180.757 17.8879C181.513 17.8838 182.256 17.6828 182.911 17.3047C183.565 16.9266 184.111 16.3844 184.493 15.7315L186.715 16.8645C185.581 18.642 183.758 20.1968 180.757 20.1968C176.38 20.1968 172.978 17.0863 172.978 12.5331ZM190.582 19.9299V5.11295H193.183V17.642H199.718V19.9299H190.582ZM203.852 19.9299V5.11295H214.01V7.40085H206.453V11.2444H213.854V13.5323H206.452V17.642H214.01V19.9299H203.852Z"
							fill="#15486B"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M36.1528 15.3677C36.1528 14.6322 35.6181 14.0314 34.6835 14.0314H30.8302V16.7024H34.6835C35.5964 16.7024 36.1528 16.1685 36.1528 15.3677ZM35.8641 9.53465C35.8641 8.82256 35.3286 8.31046 34.5496 8.31046H30.8302V10.7814H34.5488C35.3286 10.7814 35.8641 10.2693 35.8641 9.53465ZM26.9988 19.9298V5.08304H35.4181C38.3576 5.08304 39.7609 6.99756 39.7609 8.86772C39.7609 10.7145 38.6254 11.9613 37.2214 12.2726C38.8028 12.5185 40.0504 14.0314 40.0504 15.9016C40.0504 18.0613 38.6028 19.9306 35.6851 19.9306H26.9988V19.9298ZM50.3407 8.86772L48.448 14.454H52.2335L50.3407 8.86691V8.86772ZM54.0149 19.9298L53.281 17.7927H47.423L46.6883 19.9298H42.3456L47.9359 5.08304H52.7456L58.3585 19.9298H54.0149ZM71.7447 12.5177C71.7447 10.0911 70.1641 8.22256 67.7141 8.22256C65.2415 8.22256 63.6601 10.0911 63.6601 12.5177C63.6601 14.9218 65.2415 16.8137 67.7141 16.8137C70.1633 16.8137 71.7447 14.9218 71.7447 12.5177ZM59.7633 12.5177C59.7633 7.99917 63.1697 4.83868 67.7141 4.83868C72.2568 4.83868 75.6415 7.99917 75.6415 12.5177C75.6415 17.0371 72.2568 20.1976 67.7141 20.1976C63.1697 20.1976 59.7633 17.0363 59.7633 12.5177ZM88.516 15.3677C88.516 14.6322 87.9814 14.0314 87.0467 14.0314H83.1935V16.7024H87.0467C87.9596 16.7024 88.516 16.1685 88.516 15.3677ZM88.2273 9.53465C88.2273 8.82256 87.6926 8.31046 86.9128 8.31046H83.1935V10.7814H86.9128C87.6926 10.7814 88.2273 10.2693 88.2273 9.53465ZM79.3617 19.9298V5.08304H87.7814C90.7217 5.08304 92.1249 6.99756 92.1249 8.86772C92.1249 10.7145 90.9886 11.9613 89.5846 12.2726C91.166 12.5185 92.4136 14.0314 92.4136 15.9016C92.4136 18.0613 90.966 19.9306 88.0483 19.9306H79.3625L79.3617 19.9298ZM102.704 8.86772L100.811 14.454H104.597L102.704 8.86691V8.86772ZM106.379 19.9298L105.645 17.7927H99.787L99.0515 19.9298H94.7088L100.299 5.08304H105.11L110.722 19.9298H106.379ZM122.75 15.3677C122.75 14.6322 122.214 14.0314 121.28 14.0314H117.427V16.7024H121.28C122.193 16.7024 122.75 16.1685 122.75 15.3677ZM122.46 9.53465C122.46 8.82256 121.926 8.31046 121.146 8.31046H117.427V10.7814H121.146C121.926 10.7814 122.46 10.2693 122.46 9.53465ZM113.595 19.9298V5.08304H122.014C124.955 5.08304 126.358 6.99756 126.358 8.86772C126.358 10.7145 125.222 11.9613 123.818 12.2726C125.399 12.5185 126.647 14.0314 126.647 15.9016C126.647 18.0613 125.199 19.9306 122.281 19.9306H113.596L113.595 19.9298Z"
							fill="#F19B3E"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M12.1432 8.52727C12.2514 7.45015 13.549 6.66391 13.5868 6.3149C13.6304 5.91741 13.297 5.47484 12.8326 5.48599C12.4278 5.49569 11.1748 7.00128 10.6469 6.85004C10.1195 6.69978 10.3454 4.85778 10.0943 4.55724C9.95518 4.38952 9.78308 4.31002 9.468 4.30469C9.15243 4.3105 8.98034 4.38952 8.84123 4.55675C8.59018 4.85826 8.81606 6.69978 8.28864 6.85004C7.76028 7.00128 6.50772 5.49569 6.10247 5.48599C5.63858 5.47484 5.30556 5.91741 5.34919 6.3149C5.38749 6.66391 6.68416 7.45015 6.79226 8.52727C6.90035 9.60485 4.62402 14.3562 4.62402 22.2608C4.62402 23.0349 7.63813 23.6971 9.468 23.6971C11.2974 23.6971 14.3125 23.0349 14.3125 22.2608C14.3125 14.3562 12.0352 9.60429 12.1432 8.52727Z"
							fill="#15486B"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M9.46795 24.3065C8.22424 24.3099 6.99215 24.0667 5.84302 23.591C4.6939 23.1152 3.65051 22.4163 2.77322 21.5347C1.89142 20.6583 1.19226 19.6157 0.71623 18.4672C0.240204 17.3187 -0.00322728 16.0871 3.49373e-05 14.8439C-0.00335492 13.6006 0.239969 12.369 0.715913 11.2204C1.19186 10.0718 1.89096 9.02906 2.77273 8.1526C2.80457 8.12075 2.84235 8.09551 2.88394 8.07825C2.92552 8.06105 2.97009 8.05219 3.01511 8.05219C3.06011 8.05219 3.10469 8.06105 3.14627 8.07825C3.18786 8.09551 3.22565 8.12075 3.25747 8.1526C3.2893 8.18438 3.31455 8.2222 3.33177 8.26381C3.349 8.30535 3.35786 8.34994 3.35786 8.39494C3.35786 8.43994 3.349 8.48454 3.33177 8.52615C3.31455 8.56768 3.2893 8.60551 3.25747 8.63728C-0.166231 12.0595 -0.166231 17.6278 3.25747 21.0505C4.07118 21.8683 5.03901 22.5166 6.10495 22.958C7.17089 23.3993 8.31376 23.6249 9.46747 23.6216C11.8136 23.6216 14.0182 22.7087 15.6769 21.0505C19.1011 17.6278 19.1011 12.0595 15.6769 8.63728C15.6451 8.60551 15.6199 8.56768 15.6026 8.52615C15.5854 8.48454 15.5766 8.43994 15.5766 8.39494C15.5766 8.34994 15.5854 8.30535 15.6026 8.26381C15.6199 8.2222 15.6451 8.18438 15.6769 8.1526C15.7088 8.12075 15.7466 8.09551 15.7881 8.07825C15.8297 8.06105 15.8743 8.05219 15.9193 8.05219C15.9643 8.05219 16.0089 8.06105 16.0505 8.07825C16.0921 8.09551 16.1299 8.12075 16.1617 8.1526C17.0436 9.02897 17.7428 10.0717 18.2188 11.2203C18.6946 12.3689 18.938 13.6006 18.9344 14.8439C18.938 16.0871 18.6947 17.3187 18.2188 18.4673C17.7428 19.6158 17.0436 20.6584 16.1617 21.5347C15.2844 22.4162 14.2411 23.1151 13.0921 23.5909C11.943 24.0666 10.7116 24.3099 9.46795 24.3065Z"
							fill="#15486B"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M9.91343 3.71814C9.91343 3.71814 8.01566 3.96148 6.99674 2.94207C5.97879 1.92267 6.22552 0.0254095 6.22552 0.0254095C6.22552 0.0254095 8.12375 -0.217929 9.14222 0.801475C10.1601 1.82185 9.91343 3.71814 9.91343 3.71814Z"
							fill="#F19B3E"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M12.7107 0.0312474C12.7107 0.0312474 12.9541 1.92851 11.9342 2.94646C10.9144 3.96441 9.01613 3.7172 9.01613 3.7172C9.01613 3.7172 8.77274 1.82041 9.79266 0.802465C10.8135 -0.215484 12.7107 0.0312474 12.7107 0.0312474Z"
							fill="#1F598A"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M5.35544 5.08366C5.35544 5.08366 4.59052 6.17965 3.58421 6.22861C2.5779 6.2766 1.71167 5.25865 1.71167 5.25865C1.71167 5.25865 2.47658 4.16218 3.4829 4.11322C4.48921 4.06522 5.35544 5.08366 5.35544 5.08366Z"
							fill="#F19B3E"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M4.75874 1.90521C4.75874 1.90521 5.8557 2.67012 5.90466 3.67547C5.95265 4.6813 4.93373 5.54753 4.93373 5.54753C4.93373 5.54753 3.83726 4.7831 3.7883 3.77727C3.73982 2.77144 4.75874 1.90521 4.75874 1.90521Z"
							fill="#1F598A"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M14.067 5.59166C14.067 5.59166 12.9696 4.82723 12.9211 3.8214C12.8726 2.81557 13.8915 1.95032 13.8915 1.95032C13.8915 1.95032 14.9885 2.71426 15.037 3.72009C15.0855 4.72592 14.067 5.59166 14.067 5.59166Z"
							fill="#F19B3E"
						/>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M17.2463 4.99538C17.2463 4.99538 16.4809 6.09137 15.4751 6.14033C14.4688 6.18881 13.6025 5.17037 13.6025 5.17037C13.6025 5.17037 14.367 4.0739 15.3733 4.02542C16.3801 3.97694 17.2463 4.99538 17.2463 4.99538Z"
							fill="#1F598A"
						/>
					</g>
					<defs>
						<clipPath id="clip0_29_7485">
							<rect width="214.516" height="25" fill="white" />
						</clipPath>
					</defs>
				</svg>
			</div>
			<div className="md:bg-white rounded-[5px] w-full relative max-w-[750px] md:mx-auto grid grid-rows-[2.5fr_1.5fr] sm:min-h-[calc(100vh-75px)] md:flex md:flex-row">
				<form
					className="px-[16px] pt-[20px] md:py-[25px] md:w-full md:px-[65px] sm:grow-[4]"
					onSubmit={handleSubmit}
				>
					<div className="">
						<h1 className="text-[24px] font-bold">Sign In</h1>
						<p className="text-[16px] mt-[5px] text-[#333333]">
							Welcome back! It's nice to see you again.
						</p>
						<div className="pt-[25px] flex flex-col gap-[16px]">
							<div className="flex gap-[5px] flex-col">
								<label
									htmlFor="phone"
									className="font-[600] text-[14px] ml-[4px] text-[#333333]"
								>
									Phone number
								</label>
								<div className="relative">
									<select
										id="countries"
										name="countries"
										className="bg-[#f2994a] rounded-[3px] text-white absolute h-[38px] my-[2px] ml-[2px] outline-none w-[80px] px-[10px] text-[16px]"
										onChange={handleCountry}
									>
										<option value="254" defaultValue>
											254
										</option>
										<option value="256">256</option>
										<option value="263">263</option>
									</select>
									<input
										required
										id="phone"
										ref={phoneRef}
										onBlur={handlePhone}
										type="number"
										placeholder="Phone number"
										className="h-[42px]  text-[#333333] w-full outline-none pl-[90px] rounded-[5px] corner shadow-inner-custom"
									/>
								</div>
							</div>
							<div className="flex gap-[5px] flex-col mb-[20px]">
								<label
									htmlFor="referral"
									className="font-[600] text-[14px] ml-[4px] text-[#333333]"
								>
									Referral Code
								</label>
								<div className="relative">
									<input
										required
										id="referral"
										type="text"
										placeholder="Referrer Code"
										onChange={(e) =>
											setReferral(e.target.value)
										}
										className="h-[42px] w-full outline-none px-[10px] rounded-[5px] corner shadow-inner-custom"
									/>
								</div>
							</div>
							<button
								disabled={loading}
								className="text-[16px] md:w-[184px] md:mx-auto bg-[#f2994a] text-white leading-[44px] h-[44px] rounded-[5px] font-[600] flex items-center justify-center"
							>
								{!loading && "Continue"}
								{loading && (
									<div className="animate-spin rounded-full h-[16px] w-[16px] border-t-2 border-white"></div>
								)}
							</button>
							<div className="h-[20px] text-center text-[14px] text-red-400">
								{error}
							</div>
						</div>
					</div>
				</form>
				<div className="bg-white md:bg-[#f2994a] md:min-w-[270px] flex md:w-[238px] py-[25px] px-[20px] pt-[40px]">
					<div className="my-auto">
						<img
							alt="icon small"
							src="/assets/logic-small.svg"
							className="h-[50px] w-[50px] mx-auto md:hidden"
						/>
						<img
							alt="icon"
							src="/assets/logic.svg"
							className="h-[60px] w-[60px] mx-auto hidden md:block"
						/>
						<div className="flex flex-col gap-[4px] mt-[20px] md:text-white">
							<div className="text-[20px] font-[600] text-center mt-[16px]">
								Limitless Remote support
							</div>
							<p className="text-[14px] text-center">
								Everything you need on one platform. Follow up
								and keep track of your patients through our
								seamless and secure dashboard
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Login
