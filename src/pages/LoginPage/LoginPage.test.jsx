import React from 'react';
import { render } from '@testing-library/react';
import LoginPage from './LoginPage';
describe('LoginPage Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<LoginPage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
