import React from 'react';
import ResetPasswordPage from './ResetPasswordPage.jsx';
export default {
  title: 'ResetPasswordPage',
  page: ResetPasswordPage,
};
export const Default = () => <ResetPasswordPage />;
