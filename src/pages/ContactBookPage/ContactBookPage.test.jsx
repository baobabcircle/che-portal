import React from 'react';
import { render } from '@testing-library/react';
import ContactBookPage from './ContactBookPage';
describe('ContactBookPage Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<ContactBookPage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
