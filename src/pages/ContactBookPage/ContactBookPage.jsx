import React, { useEffect, useState, useRef } from "react"
import ContactItem from "../../components/ContactItem/ContactItem"
import CallScreen from "../../components/CallScreen/CallScreen"
import styles from "./ContactBookPage.module.scss"
const ContactBookPage = () => {
	const [token] = useState(
		"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiN2JjNThjNTUtMWFmZC00YjVhLTlhNzEtN2QxMWQ2MTFhZDQxIiwidXNlcm5hbWUiOiJncm91cGFkbWluMkBjYXJlZ2l2ZXIuY29tIiwic3RhdHVzIjoiQUNUSVZFIiwidHlwZSI6Imdyb3VwLWFkbWluIiwicm9sZXMiOlsiR1JPVVBfTUFOQUdFTUVOVF9BRE1JTiJdLCJwZXJtaXNzaW9ucyI6W119LCJpYXQiOjE2MDU3NjIwMjIsImV4cCI6MTYwODM1NDAyMn0.SBfLq-aPMco3gdqM4JAyJ8xMc_0cxv1-JZ9mgyXhFx0"
	)
	const [url] = useState(
		`https://api.baobabcircle.com/care-giver/patients/search`
	)
	const [country, setCountry] = useState("254")
	const [contact, setContact] = useState("")
	const [contacts, setContacts] = useState("")
	const [phone, setPhone] = useState("")
	const [phoneError, setPhoneError] = useState("")
	const [loading, setLoading] = useState(false)
	const phoneRef = useRef(null)

	const handleCountry = (event) => {
		setCountry(event.target.value)
	}
	const callHandler = (tel) => {
		setContact(tel)
	}
	const handlePhone = (event) => {
		event.preventDefault()
		setLoading(true)
		setPhone(event.target.value)
		if (event.target.value.length < 7 && event.target.value.length >= 1) {
			setLoading(false)
			setContacts("")
			setPhoneError("Enter a complete number")
		} else if (event.target.value.length === 0) {
			setLoading(false)
			setContacts("")
			setPhoneError("")
			return
		} else {
			const number = country + phone
			fetch(url, {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					Authorization: "Bearer " + token,
				},
				body: JSON.stringify({
					msisdn: number,
				}),
			})
				.then((response) => response.json())
				.then((result) => {
					if (result.message === "RESULTS FOUND.") {
						setContacts(result.data.patients)
						setLoading(false)
					} else {
						setLoading(false)
						setPhoneError(
							"No contacts found,please check your number and try again"
						)
					}
				})
				.catch((error) => {
					console.log(error)
					setLoading(false)
					setPhoneError("Something went wrong, please try again")
				})
			setPhoneError("")
		}
	}
	useEffect(() => {
		return function () {
			window.scrollTo(0, 0)
		}
	}, [])
	return (
		<div className={styles.contact_book_page}>
			<div className={styles.search_input_container}>
				<div className={styles.search_input}>
					<div className={styles.icon}>
						<svg
							viewBox="0 0 20 20"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="M12.9167 11.6667H12.2583L12.025 11.4417C12.8699 10.4617 13.3343 9.21058 13.3333 7.91667C13.3333 6.84535 13.0157 5.7981 12.4205 4.90733C11.8253 4.01656 10.9793 3.3223 9.98954 2.91232C8.99977 2.50235 7.91066 2.39508 6.85993 2.60408C5.8092 2.81309 4.84404 3.32897 4.08651 4.08651C3.32897 4.84404 2.81309 5.8092 2.60408 6.85993C2.39508 7.91066 2.50235 8.99977 2.91232 9.98954C3.3223 10.9793 4.01656 11.8253 4.90733 12.4205C5.7981 13.0157 6.84535 13.3333 7.91667 13.3333C9.25834 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667V11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z"
								fill="#A1A1A1"
							/>
						</svg>
					</div>
					<select
						id="countries"
						name="countries"
						onChange={handleCountry}
					>
						<option value="254" defaultValue>
							+254
						</option>
						<option value="256">+256</option>
						<option value="263">+263</option>
					</select>
					<input type="tel" placeholder="Phone number" />
					<input
						required
						id="phone"
						ref={phoneRef}
						type="number"
						placeholder="Phone number"
						className={phoneError ? styles.input_error : ""}
						onChange={(e) => {
							setPhone(e.target.value)
							if (phone.length >= 6) {
								handlePhone(e)
							}
						}}
						onBlur={handlePhone}
					/>
				</div>
			</div>
			<ul className={styles.contact_list}>
				{contacts &&
					contacts.map((contact) => {
						return (
							<ContactItem
								onClick={() => callHandler(contact.msisdn)}
								contact={contact}
								key={contact.id}
							/>
						)
					})}
				{!contacts && (
					<div className={styles.message}>
						<p>{phoneError}</p>
					</div>
				)}
				{loading && (
					<div className="loader-container">
						<svg
							className="loader"
							version="1.1"
							id="loader-1"
							xmlns="http://www.w3.org/2000/svg"
							x="0px"
							y="0px"
							width="2.50rem"
							height="2.50rem"
							viewBox="0 0 40 40"
							enableBackground="new 0 0 40 40"
						>
							<path
								opacity="0.2"
								d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"
							/>
							<path
								d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
    C22.32,8.481,24.301,9.057,26.013,10.047z"
							>
								<animateTransform
									attributeType="xml"
									attributeName="transform"
									type="rotate"
									from="0 20 20"
									to="360 20 20"
									dur="0.5s"
									repeatCount="indefinite"
								/>
							</path>
						</svg>
					</div>
				)}
			</ul>
			{contact && (
				<CallScreen
					onEnd={() => callHandler(null)}
					number={contact}
				></CallScreen>
			)}
		</div>
	)
}
export default ContactBookPage
