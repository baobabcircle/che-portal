import React from "react"
import { render } from "@testing-library/react"
import NotificationsPage from "./NotificationsPage"
describe("NotificationsPage Page", () => {
	test("it should match the snapshot", () => {
		const { asFragment } = render(<NotificationsPage />)
		expect(asFragment()).toMatchSnapshot()
	})
})
