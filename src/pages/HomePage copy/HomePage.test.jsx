import React from 'react';
import { render } from '@testing-library/react';
import HomePage from './HomePage';
describe('HomePage Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<HomePage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
