import React, { useEffect } from "react"
import { useNavigate } from "react-router"
import styles from "./HomePage.module.scss"
import TileButton from "../../components/TileButton/TileButton"
import HomeHeader from "../../components/HomeHeader/HomeHeader"
const HomePage = () => {
	const navigate = useNavigate()
	useEffect(() => {
		return function () {
			window.scrollTo(0, 0)
		}
	}, [])

	return (
		<div className={styles.container}>
			<HomeHeader />
			<div className={styles.hr_line}></div>
			<div className={styles.tiles}>
				<TileButton onClick={() => navigate("/contact-book")} />
			</div>
		</div>
	)
}
export default HomePage
