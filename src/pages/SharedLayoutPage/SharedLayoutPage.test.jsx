import React from 'react';
import { render } from '@testing-library/react';
import SharedLayoutPage from './SharedLayoutPage';
describe('SharedLayoutPage Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<SharedLayoutPage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
