import React from 'react';
import { render } from '@testing-library/react';
import ConfirmationPage from './ConfirmationPage';
describe('ConfirmationPage Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<ConfirmationPage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
