const fs = require("fs")
const { page, story, test, barrel } = require("./page_templates.js")

// grab page name from terminal argument
const [name] = process.argv.slice(2)
if (!name) throw new Error("You must include a page name.")

const dir = `./src/pages/${name}/`

// throw an error if the file already exists
if (fs.existsSync(dir)) throw new Error("A page with that name already exists.")

// create the folder
fs.mkdirSync(dir)

function writeFileErrorHandler(err) {
	if (err) throw err
}

// page.jsx
fs.writeFile(`${dir}/${name}.jsx`, page(name), writeFileErrorHandler)
// page.scss
fs.writeFile(`${dir}/${name}.module.scss`, "", writeFileErrorHandler)
// storybook.jsx
fs.writeFile(`${dir}/${name}.stories.jsx`, story(name), writeFileErrorHandler)
// test.jsx
fs.writeFile(`${dir}/${name}.test.jsx`, test(name), writeFileErrorHandler)
// index.jsx
fs.writeFile(`${dir}/index.jsx`, barrel(name), writeFileErrorHandler)

////////////////
/// Optional ///
////////////////

// insert new page into 'pages/index.ts file
fs.readFile("./src/pages/index.jsx", "utf8", function (err, data) {
	if (err) throw err

	// grab all pages and combine them with new page
	const currentComponents = data.match(/(?<=import )(.*?)(?= from)/g)
	const newPages = [name, ...currentComponents].sort()

	// create the import and export statements
	const importStatements = newPages
		.map((importName) => `import ${importName} from './${importName}';\n`)
		.join("")
	const exportStatements = `export {\n${newPages
		.map((page) => `  ${page},\n`)
		.join("")}};\n`

	const fileContent = `${importStatements}\n${exportStatements}`

	fs.writeFile(`./src/pages/index.jsx`, fileContent, writeFileErrorHandler)
})
