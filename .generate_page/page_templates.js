// page.jsx
exports.page = (name) => `import React,{useEffect} from 'react';
import styles from './${name}.module.scss';
const ${name} = () => {
	useEffect(() => {
		return function () {
			window.scrollTo(0, 0)
		}
	}, [])
  return <div>Hello 👋, I am a ${name} page.</div>;
};
export default ${name};
`

// page.stories.jsx
exports.story = (name) => `import React from 'react';
import ${name} from './${name}.jsx';
export default {
  title: '${name}',
  page: ${name},
};
export const Default = () => <${name} />;
`

// page.test.jsx
exports.test = (name) => `import React from 'react';
import { render } from '@testing-library/react';
import ${name} from './${name}';
describe('${name} Page', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<${name} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
`

// index.ts
exports.barrel = (name) => `import ${name} from './${name}';
export default ${name};
`
